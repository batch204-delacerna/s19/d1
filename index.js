// console.log("Hello world");

/*
	Selection Control Structures
		- sorts out whether the statement/s are to be executed based on the condition whether it is true or false

	if else statement
	switch statement

	if .. else statement

	Syntax:
		if(condition)
		{
			//statement
		}
		else
		{
			//statement
		}
*/

//IF Statement
	// executes a statement if a specified condition is true
	// can stand alone even without the else statement
	/*
		Syntax:
			if(condition)
			{
				code block
			}
	*/

let numA = -1;

if (numA < 0)
{
	console.log("Hello!");
}

console.log(numA < 0);

if (numA > 0) 
{
	console.log("This statement will not be printed!");
}

console.log(numA > 0);

// Another example
let city = "New York";

if (city === "New York")
{
	console.log("Welcome to New York City!");
}

// Else If
/*
	Executes a statement if previous conditions are false and if the specified condition is true.
	Else if class is OPTIONAL and can be added to capture additional conditions to change the flow of a program.
*/

let numB = 1;

if(numA > 0)
{
	console.log("Hello from elseif");
} 
else if (numB > 0)
{
	console.log("World!")
}

// Another Example
city = "Tokyo";

if (city === "New York") 
{
	console.log("Welcome to New York City");	
}
else if (city === "Tokyo") 
{
	console.log("Welcome to Tokyo");	
};

// Else statement
/*
	Executes a statement if all other conditions are false
	The "else" statement is optional and can be added to capture any other result to change the flow of a program.
*/

if (numA > 0 ) 
{
	console.log("Hello");
}
else if (numB === 0) 
{
	console.log("Hello");
}
else
{
	console.log("Again!");
}

// Another example
// let age = 20;

// if (age<=18)
// {
// 	console.log("Not allowed to Drink!");
// }
// else
// {
// 	console.log("Matanda ka na, shot na!");	
// }

/*
	Mini-Activity:
		Create a conditional statement that if height is below 150, display "Did not pass the minimum height requirement"
		If above 150, display "Passed the minimum height requirement."

		**Stretch Goal:
			Put it inside a function.

*/

function isTallEnough(heightInCM)
{
	if (heightInCM < 150)
	{
		console.log("Did not pass the minimum height requirement");
	}
	else
	{
		console.log("Passed the minimum height requirement.");
	}
}

isTallEnough(149);
isTallEnough(175);

// If, else if and else statement with Function
let message = 'No message.';
console.log(message);

function determineTyphoonIntensity(windSpeed)
{
	if (windSpeed < 30)
	{
		return 'Not a typhoon yet.';
	}
	else if (windSpeed <= 61) 
	{
		return 'Tropical depression detected.';
	}
	else if (windSpeed >= 62 && windSpeed <= 88)
	{
		return 'Tropical Storm detected.';
	}
	else
	{
		return 'Typhoon detected.';
	}
}

message = determineTyphoonIntensity(70);
console.log(message);

// let windSpeed = prompt("Enter the wind speed: ");
// message = determineTyphoonIntensity(windSpeed);
// console.log(message);

// message = determineTyphoonIntensity(prompt("Enter the wind speed: "));
// console.log(message);

if (message === 'Tropical Storm detected.')
{
	console.warn(message);
}

// Truthy and Falsy

/*
	- In JavaScript a "Truthy" value is a value that is considered true when encountered in a Boolean context.
	- Values are considered true unless difined otherwise.
	- Falsy values/exceptions for truthy
	1. false
	2. 0
	3. -0
	4. ""
	5. null
	6. undefined
	7. NaN

*/

// Truthy Examples
if (true) 
{
	console.log("Truthy!");
}

if (1) 
{
	console.log("Truthy!");
}

if ([])
{
	console.log("Truthy!");
}

// Falsy Examples
if (false)
{
	console.log("Falsy!");
}

if (0)
{
	console.log("Falsy!");
}

if (undefined)
{
	console.log("Falsy!");
}

// Conditional (Ternary) Operator
/*
	The condition (ternary) operator takes in three operands:
	1. condition
	2. expression to execute if the condition is truthy
	3. expression to execute if the condition is falsy

	- Can be used as an alternative to an "if else" statement
	- Ternary Operators have an implicit "return" statement meaning that without the "return" keyword, the resulting expressions can be stored in a variable
	Syntax:
		(expression) ? ifTrue : ifFalse;
*/

// Singe Statement Execution
let ternaryResult = (1 < 18) ? true : false;
console.log("Result of the Ternary Operator: " + ternaryResult);

// Multiple Statement Execution
// let name = prompt("Enter your name: ");

function isOfLegalAge()
{
	//name = 'John';
	return 'You are of the legal age limit';
}

function isOfUnderAge()
{
	//name = 'Jane';
	return 'You are under the age limit';
}

// The "parseInt" function converts the input received into a number data type
// let age = parseInt(prompt("What is your age?"));
// console.log(age);

// let legalAge = (age > 18) ? isOfLegalAge() : isOfUnderAge();
// console.log("Result of the Ternary Operator in Functions: " + legalAge + ", " + name);

// Switch Statement
/*
	Can be used as an alternative to an if .. else statement where the data used in the condition is of an expected input

	Syntax:
		switch(expression)
		{
			case<value>;
				statement;
				break;
			default;
				statement;
				break;
		}
*/

// MONDAY monday
// let day = prompt("What day of the week is it today?").toLowerCase();
// console.log(day)

// switch(day)
// {
// 	case 'monday':
// 		console.log("The color of the day is red");
// 		break;
// 	case 'tuesday':
// 		console.log("The color of the day is orange");
// 		break;
// 	case 'wednesday':
// 		console.log("The color of the day is yellow");
// 		break;
// 	case 'thursday':
// 		console.log("The color of the day is green");
// 		break;
// 	case 'friday':
// 		console.log("The color of the day is blue");
// 		break;
// 	case 'saturday':
// 		console.log("The color of the day is indigo");
// 		break;
// 	case 'sunday':
// 		console.log("The color of the day is violet");
// 		break;
// 	default:
// 		console.log("Please input a valid day!");
// 	break;
// }

// Try-Cath-Finally Statement
	/*
		try catch are commonly used for error handling
	*/

function showIntensityAlert(windSpeed)
{
	try
	{
		// attempt to execute a code
		alerat(determineTyphoonIntensity(windSpeed));
	}
	catch(error)
	{
		console.warn(error);
	}
	finally
	{
		alert('Intensity updates will show new alert');
	}
}

showIntensityAlert(56);